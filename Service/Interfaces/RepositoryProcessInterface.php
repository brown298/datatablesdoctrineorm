<?php
namespace Brown298\DataTablesDoctrineORM\Service\Interfaces;

use Doctrine\ORM\EntityRepository;

/**
 * Interface RepositoryProcessInterface
 * @package Brown298\DataTablesDoctrineORM\Service\Interfaces
 */
interface RepositoryProcessInterface
{
    /**
     * @param EntityRepository $repository
     * @return mixed
     */
    public function setRepository(EntityRepository $repository);

    /**
     * getRepository
     *
     * @return Processor\Doctrine\ORM\EntityRepository|null
     */
    public function getRepository();

    /**
     * Adds support for magic finders.
     *
     * @param string $method
     * @param array $arguments
     *
     * @throws \Brown298\DataTablesModels\Exceptions\ProcessorException
     * @return array|object The found entity/entities.
     *
     */
    public function __call($method, $arguments);

    /**
     * findAll
     *
     * builds a findAll Query
     *
     * @throws \Brown298\DataTablesModels\Exceptions\ProcessorException
     */
    public function findAll();

    /**
     * findBy
     *
     * builds a findBy query
     *
     * @param array $criteria
     * @param array $orderBy
     * @param null $limit
     * @param null $offset
     *
     * @throws \Brown298\DataTablesModels\Exceptions\ProcessorException
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);
}
