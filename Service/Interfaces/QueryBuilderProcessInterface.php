<?php
namespace Brown298\DataTablesDoctrineORM\Service\Interfaces;

use Doctrine\ORM\QueryBuilder;

/**
 * Interface QueryBuilderProcessInterface
 * @package Brown298\DataTablesDoctrineORM\Service
 */
interface QueryBuilderProcessInterface
{
    /**
     * @param QueryBuilder $queryBuilder
     * @return mixed
     */
    public function setQueryBuilder(QueryBuilder $queryBuilder);

    /**
     * getQueryBuilder
     *
     * @return mixed
     */
    public function getQueryBuilder();
}
