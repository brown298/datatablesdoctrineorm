<?php
namespace Brown298\DataTablesDoctrineORM\Test\DataTable;

use Brown298\DataTablesDoctrineORM\Model\DataTable\AbstractRepositoryDataTable;
use Brown298\DataTablesDoctrineORM\Model\DataTable\RepositoryDataTableInterface;

/**
 * Class RepositoryDataTable
 *
 * @package Brown298\DataTablesDoctrineORM\Test\DataTable
 * @author  John Brown <brown.john@gmail.com>
 */
class RepositoryDataTable extends AbstractRepositoryDataTable implements RepositoryDataTableInterface
{

}
