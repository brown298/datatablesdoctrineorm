<?php
namespace Brown298\DataTablesDoctrineORM\Tests\Model;

use Brown298\DataTablesDoctrineORM\Test\DataTable\RepositoryDataTable;
use Phake;
use \Brown298\TestExtension\Test\AbstractTest;

/**
 * Class RepositoryDataTableTest
 *
 * @package Brown298\DataTablesDoctrineORM\Tests\Model
 * @author  John Brown <brown.john@gmail.com>
 */
class RepositoryDataTableTest extends AbstractTest
{

    /**
     * @Mock
     * @var \Psr\Http\Message\ServerRequestInterface
     */
    protected $request;

    /**
     * @Mock
     * @var \Brown298\DataTablesDoctrineORM\Service\ServerProcessService
     */
    protected $dataTablesService;

    /**
     * @var \Brown298\DataTablesDoctrineORM\Test\DataTable\RepositoryDataTable
     */
    protected $dataTable;

    /**
     * @Mock
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @Mock
     * @var \Brown298\DataTablesDoctrineORM\Service\ServerProcessService
     */
    protected $service;

    /**
     * @Mock
     * @var Doctrine\ORM\QueryBuilder
     */
    protected $queryBuilder;

    /**
     * @Mock
     * @var Doctrine\ORM\EntityRepository
     */
    protected $repository;


    /**
     * setUp
     *
     */
    public function setUp()
    {
        parent::setUp();

        $this->dataTable = new RepositoryDataTable();
        $this->dataTable->setServerProcessService($this->service);
    }

    /**
     * testCreate
     *
     */
    public function testCreate()
    {
        $this->assertInstanceOf('\Brown298\DataTablesDoctrineORM\Test\DataTable\RepositoryDataTable', $this->dataTable);
        $this->assertInstanceOf('\Brown298\DataTablesDoctrineORM\Model\DataTable\RepositoryDataTableInterface', $this->dataTable);
        $this->assertInstanceOf('\Brown298\DataTablesDoctrineORM\Model\DataTable\QueryBuilderDataTableInterface', $this->dataTable);
    }

    /**
     * testGetSetRepository
     *
     */
    public function testGetSetRepository()
    {
        $this->dataTable->setRepository($this->repository);
        $this->assertEquals($this->repository, $this->dataTable->getRepository());
    }


    /**
     * testFindByThrowsExceptionWithoutContainer
     *
     * @expectedException \Brown298\DataTablesModels\Exceptions\ProcessorException
     */
    public function testFindByThrowsExceptionWithoutContainer()
    {
        $this->dataTable->setServerProcessService(null);
        $this->dataTable->findBy(array());
    }

    /**
     * testFindByThrowsExceptionWithoutRepository
     *
     * @expectedException \Brown298\DataTablesModels\Exceptions\ProcessorException
     */
    public function testFindByThrowsExceptionWithoutRepository()
    {
        $this->dataTable->findBy(array());
    }

    /**
     * testFindByRunsOnRepository
     *
     */
    public function testFindByRunsOnRepository()
    {
        $this->dataTable->setRepository($this->repository);
        $this->dataTable->findBy(array());

        Phake::verify($this->service)->setRepository($this->repository);
        Phake::verify($this->service)->findBy(array(), null, null, null);
    }

    /**
     * testFindAllThrowsExceptionWithoutContainer
     *
     * @expectedException \Brown298\DataTablesModels\Exceptions\ProcessorException
     */
    public function testFindAllThrowsExceptionWithoutContainer()
    {
        $this->dataTable->setServerProcessService(null);
        $this->dataTable->findAll(array());
    }

    /**
     * testFindAllThrowsExceptionWithoutRepository
     *
     * @expectedException \Brown298\DataTablesModels\Exceptions\ProcessorException
     */
    public function testFindAllThrowsExceptionWithoutRepository()
    {
        $this->dataTable->findAll(array());
    }

    /**
     * testFindAllRunsService
     *
     */
    public function testFindAllRunsService()
    {
        $this->dataTable->setRepository($this->repository);
        $this->dataTable->findAll();

        Phake::verify($this->service)->setRepository($this->repository);
        Phake::verify($this->service)->findAll();
    }

    /**
     * testGenericThrowsExceptionWithoutContainer
     *
     * @expectedException \Brown298\DataTablesModels\Exceptions\ProcessorException
     */
    public function testGenericThrowsExceptionWithoutContainer()
    {
        $this->dataTable->setServerProcessService(null);
        $this->dataTable->aaaah();
    }

    /**
     * testGenericThrowsExceptionWithoutRepository
     *
     * @expectedException \Brown298\DataTablesModels\Exceptions\ProcessorException
     */
    public function testGenericThrowsExceptionWithoutRepository()
    {
        $this->dataTable->aaaah();
    }

    /**
     * testGenericByRunsOnRepository
     *
     */
    public function testGenericByRunsOnRepository()
    {
        $this->dataTable->setRepository($this->repository);
        $this->dataTable->aaaah();

        Phake::verify($this->service)->setRepository($this->repository);
        Phake::verify($this->service)->aaaah(Phake::anyParameters());
    }

    /**
     * testGetDataNull
     */
    public function testGetDataNull()
    {
        $this->assertNull($this->dataTable->getData($this->request));
    }

    /**
     * testGetData
     */
    public function testGetData()
    {
        $expectedResult = 'test';
        Phake::when($this->service)->process(Phake::anyParameters())->thenReturn($expectedResult);
        $this->dataTable->setRepository($this->repository);
        $this->dataTable->setRepository($this->repository);

        $result = $this->dataTable->getData($this->request);

        $this->assertEquals($expectedResult, $result);
        Phake::verify($this->service)->setRequest($this->request);
        Phake::verify($this->service)->setRepository($this->repository);

    }

} 