<?php
namespace Brown298\DataTablesDoctrineORM\Model\DataTable;

use Brown298\DataTablesModels\Exceptions\ProcessorException;
use Brown298\DataTablesModels\Exceptions\ResourceNotFoundException;
use Brown298\DataTablesModels\Model\DataTable\DataTableInterface;
use Brown298\DataTablesDoctrineORM\Service\Interfaces\QueryBuilderProcessInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Psr\Http\Message\RequestInterface;

/**
 * Class AbstractQueryBuilderDataTable
 *
 * @package Brown298\DataTablesDoctrineORM\Model\DataTable
 * @author  John Brown <brown.john@gmail.com>
 */
abstract class AbstractQueryBuilderDataTable extends AbstractDataTable implements DataTableInterface
{
    /**
     * @var QueryBuilder
     */
    protected $queryBuilder = null;

    /**
     * @var bool
     */
    public $hydrateObjects = false;

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $repository;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * getData
     *
     * override this function to return a raw data array
     *
     * @param RequestInterface $request
     * @param null    $dataFormatter
     *
     * @return JsonResponse
     */
    public function getData(RequestInterface $request, $dataFormatter = null)
    {
        $this->queryBuilder = $this->getQueryBuilder($request);

        return $this->getDataByQueryBuilder($request, $this->queryBuilder, $dataFormatter);
    }

    /**
     * getObject Value
     *
     * allows for relations based on things like faq.createdBy.id
     *
     * @param $row
     * @param $source
     * @return string
     */
    protected function getObjectValue($row, $source)
    {
        return parent::getObjectValue($row, $source);
    }

    /**
     * getDataByQueryBuilder
     *
     * uses a query builder to get the required data
     *
     * @param RequestInterface $request
     * @param QueryBuilder $qb
     * @param null $dataFormatter
     * @return JsonResponse
     * @throws ProcessorException
     */
    protected function getDataByQueryBuilder(RequestInterface $request, QueryBuilder $qb, $dataFormatter = null)
    {
        if (! $this->serverProcessService instanceof QueryBuilderProcessInterface ) {
            throw new ProcessorException("Server process service does not properly implement QueryBuilderProcessInterface");
        }

        if ($this->serverProcessService->getRequest() === null) {
            $this->serverProcessService->setRequest($request);
        }

        $this->serverProcessService->setQueryBuilder($qb);
        if ($this->serverProcessService->getColumns() === null || count($this->serverProcessService->getColumns()) !== count($this->columns)) {
            $this->serverProcessService->setColumns($this->columns);
        }

        return $this->execute($this->serverProcessService, $dataFormatter);
    }


    /**
     * execute
     *
     * @param $service
     * @param $formatter
     */
    public function execute($service, $formatter)
    {
        return $service->process($formatter, $this->hydrateObjects);
    }

    /**
     * setQueryBuilder
     *
     * @param QueryBuilder $qb
     */
    public function setQueryBuilder(QueryBuilder $qb)
    {
        $this->queryBuilder = $qb;
    }

    /**
     * getQueryBuilder
     *
     * override this function to return a query builder
     *
     * @param RequestInterface $request
     * @return QueryBuilder|null
     * @throws ResourceNotFoundException
     */
    public function getQueryBuilder(RequestInterface $request = null)
    {
        if ($this->queryBuilder !== null) {
            return $this->queryBuilder;
        }

        if ($this->repository === null) {
            throw new ResourceNotFoundException('You must provide an entity repository to use the DataTables Entity');
        }

        if (!is_array($this->metaData) || !isset($this->metaData['table'])) {
            throw new ResourceNotFoundException('Metadata does not define a table could not generate query builder for data table');
        }

        // use metadata if possibile
        if (is_array($this->metaData) && isset($this->metaData['table']) && $this->metaData['table']->queryBuilder !== null) {
            $function = $this->metaData['table']->queryBuilder;

            if(method_exists($this->repository, $function)) {
                return $this->repository->$function($request);
            }
        } elseif($this->metaData['table']->entity !== null){
            $tokens = explode('\\', $this->metaData['table']->entity);
            $alias  = array_pop($tokens);
            return $this->repository->createQueryBuilder($alias);
        }

        throw new \RuntimeException('Could not generate query builder for data table');
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param \Doctrine\ORM\EntityRepository $repository
     */
    public function setRepository(EntityRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function setEm($em)
    {
        $this->em = $em;
    }
}
