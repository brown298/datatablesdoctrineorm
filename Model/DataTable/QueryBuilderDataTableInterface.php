<?php
namespace Brown298\DataTablesDoctrineORM\Model\DataTable;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Psr\Http\Message\RequestInterface;
use Brown298\DataTablesModels\Model\DataTable\DataTableInterface;

/**
 * Interface QueryBuilderDataTableInterface
 *
 * @package Brown298\DataTablesDoctrineORM\Model\DataTable
 * @author  John Brown <brown.john@gmail.com>
 */
interface QueryBuilderDataTableInterface extends DataTableInterface
{
    /**
     * execute
     *
     * @param $service
     * @param $formatter
     *
     * @return mixed
     */
    public function execute($service, $formatter);

    /**
     * setQueryBuilder
     *
     * @param QueryBuilder $qb
     *
     * @return mixed
     */
    public function setQueryBuilder(QueryBuilder $qb);

    /**
     * getQueryBuilder
     *
     * @param RequestInterface $request
     *
     * @return mixed
     */
    public function getQueryBuilder(RequestInterface $request = null);

    /**
     * setRepository
     * @param EntityRepository $repository
     * @return mixed
     */
    public function setRepository(EntityRepository $repository);

    /**
     * getRepository
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository();
}
