<?php
namespace Brown298\DataTablesDoctrineORM\Model\DataTable;

use Symfony\Component\Form\FormFactoryInterface;
use Psr\Http\Message\RequestInterface;

/**
 * Class FormFilterDataTableInterface
 *
 * @package Brown298\DataTablesDoctrineORM\Model\DataTable
 * @author John Brown <john.brown@partnerweekly.com>
 */
interface FormFilterDataTableInterface
{

    /**
     * getFilterForm
     *
     * @param RequestInterface $request
     *
     * @return mixed
     */
    public function getFilterForm(RequestInterface $request);

    /**
     * getFormFactory
     *
     * @return mixed
     */
    public function getFormFactory();

    /**
     * setFormFactory
     *
     * @param FormFactoryInterface $formFactory
     *
     * @return mixed
     */
    public function setFormFactory(FormFactoryInterface $formFactory = null);
}
